package org.jobm.service.system;

import org.jobm.UsersJobs;
import org.jobm.base.CommonService;

import java.util.List;

/**
 * @author jinjin
 * @date 2020-09-25
 */
public interface UsersJobsService extends CommonService<UsersJobs> {
    List<Long> queryUserIdByJobId(Long id);

    List<Long> queryJobIdByUserId(Long id);

    boolean removeByUserId(Long id);

    boolean removeByJobId(Long id);
}
