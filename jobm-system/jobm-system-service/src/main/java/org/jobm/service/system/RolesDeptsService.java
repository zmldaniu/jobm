package org.jobm.service.system;

import org.jobm.RolesDepts;
import org.jobm.base.CommonService;

import java.util.List;

/**
 * @author jinjin
 * @date 2020-09-25
 */
public interface RolesDeptsService extends CommonService<RolesDepts> {

    List<Long> queryDeptIdByRoleId(Long id);

    List<Long> queryRoleIdByDeptId(Long id);

    boolean removeByRoleId(Long id);

    boolean removeByDeptId(Long id);
}
