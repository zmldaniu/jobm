package org.jobm.mapper;

import org.jobm.UsersJobs;
import org.jobm.base.CommonMapper;
import org.springframework.stereotype.Repository;

/**
 * @author jinjin
 * @date 2020-09-25
 */
@Repository
public interface UsersJobsMapper extends CommonMapper<UsersJobs> {

}
