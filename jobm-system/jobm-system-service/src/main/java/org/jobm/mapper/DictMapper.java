package org.jobm.mapper;

import org.jobm.Dict;
import org.jobm.base.CommonMapper;
import org.springframework.stereotype.Repository;

/**
 * @author jinjin
 * @date 2020-09-24
 */
@Repository
public interface DictMapper extends CommonMapper<Dict> {

}
