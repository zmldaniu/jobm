package org.jobm.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jobm.DictDetail;
import org.jobm.base.CommonMapper;
import org.apache.ibatis.annotations.Param;
import org.jobm.dto.system.DictDetailDto;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author jinjin
* @date 2020-09-24
*/
@Repository
public interface DictDetailMapper extends CommonMapper<DictDetail> {

    List<DictDetailDto> getDictDetailsByDictName(@Param("dictName") String dictName);
    IPage<DictDetailDto> getDictDetailsByDictName(@Param("dictName") String dictName, IPage<DictDetailDto> page);
}
