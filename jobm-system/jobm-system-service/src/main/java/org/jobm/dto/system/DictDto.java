package org.jobm.dto.system;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jobm.base.CommonDto;

import java.io.Serializable;


/**
* @author jinjin
* @date 2020-09-24
*/
@Setter
@Getter
@NoArgsConstructor
public class DictDto extends CommonDto implements Serializable {

    private Long id;

    //     private List<DictDetailDto> dictDetails;

    private String name;

    private String description;
}
