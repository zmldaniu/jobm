/*
 *  Copyright 2019-2020 zml
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.jobm.dto.system;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import java.io.Serializable;



/**
* @author zml
* @date 2019-6-10 16:32:18
*/
@Setter
@Getter
@NoArgsConstructor
public class DeptSmallDto implements Serializable {

    private Long id;
    private String name;

    public DeptSmallDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
