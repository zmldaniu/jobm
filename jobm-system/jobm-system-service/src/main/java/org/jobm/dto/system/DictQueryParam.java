package org.jobm.dto.system;

import lombok.Getter;
import lombok.Setter;
import org.jobm.annotation.Query;


/**
* @author jinjin
* @date 2020-09-24
*/
@Setter
@Getter
public class DictQueryParam{

    @Query(blurry = "name,description")
    private String blurry;

    /** 精确 */
    @Query
    private Long dictId;

    /** 模糊 */
    @Query(type = Query.Type.INNER_LIKE)
    private String name;
}
