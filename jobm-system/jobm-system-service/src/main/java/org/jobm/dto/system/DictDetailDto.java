package org.jobm.dto.system;

import lombok.Getter;
import lombok.Setter;


import java.io.Serializable;
import java.util.Date;

/**
* @author jinjin
* @date 2020-09-24
*/
@Setter
@Getter
public class DictDetailDto implements Serializable {

    private Long id;

    private DictSmallDto dict;

    private String label;

    private String value;

    private Integer dictSort;

    private String createBy;

    private String updateBy;

    private Date createTime;

    private Date updateTime;
}
