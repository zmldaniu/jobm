package org.jobm.dto.system;

import lombok.Getter;
import lombok.Setter;
import org.jobm.annotation.Query;


/**
* @author jinjin
* @date 2020-09-24
*/
@Setter
@Getter
public class DictDetailQueryParam{

    private String dictName;

    /** 精确 */
    @Query
    private Long detailId;

    /** 精确 */
    @Query
    private Long dictId;

    /** 模糊 */
    @Query(type = Query.Type.INNER_LIKE)
    private String label;
}
