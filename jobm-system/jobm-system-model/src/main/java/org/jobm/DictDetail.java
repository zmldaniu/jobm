package org.jobm;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.jobm.base.CommonEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author zml
 * @date 2020-09-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_dict_detail")
public class DictDetail extends CommonEntity<DictDetail> implements Serializable {

    @ApiModelProperty(value = "ID")
    @TableId(value = "detail_id", type = IdType.AUTO)
    private Long id;

    @NotNull
    private Long dictId;


//    @ApiModelProperty(value = "字典id")
//    @NotNull
//    @TableField(exist = false)
//    private DictSmallDto dict;

    @ApiModelProperty(value = "字典标签")
    @NotBlank
    private String label;

    @ApiModelProperty(value = "字典值")
    @NotBlank
    private String value;

    @ApiModelProperty(value = "排序")
    private Integer dictSort;

    public void copyFrom(DictDetail source) {
        BeanUtil.copyProperties(source, this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
