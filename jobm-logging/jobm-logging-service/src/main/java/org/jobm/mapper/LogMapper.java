package org.jobm.mapper;

import org.jobm.base.CommonMapper;
import org.jobm.entity.Log;
import org.springframework.stereotype.Repository;

/**
 * @author jinjin
 * @date 2020-09-27
 */
@Repository
public interface LogMapper extends CommonMapper<Log> {

}
